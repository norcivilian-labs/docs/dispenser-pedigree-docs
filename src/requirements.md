# Requirements

# pedigree
## shaft scout multiply 
 - user must see pedigree graph 
## road broom film
 - user must download pedigree graph 
## glass series field 
 - user must edit pedigree record
## hold morning flat
 - user must edit pedigree record 
## already elephant match
 - user must delete pedigree record
## nasty wonder fade
 - user must export gedcom
## napkin jeans acid
 - user must import gedcom
## offer person super
 - user must update dataset branch metadata for pedigree
## onion lucky polar
 - user can regulate the depth of the tree and choose it’s center family

# basic csvs requirements
## little patient night
- user must import a dataset with git clone
## talent rabbit region
- user must export a dataset with git push
## elegant divert glad
- user must authenticate with git HTTP smart service
## cream piano tooth
- user must authenticate with git HTTP dumb service
## sort multiply rice
- user must authenticate with gitlab over OAuth
## exercise fragile cage
- user must authenticate with github over OAuth
## riot side foil
- user must authenticate with gitea over OAuth
## limb renew scale
- desktop user must open dataset locally
## ribbon second oak
- desktop user must open multiple windows
## detect victory grow
- browser user must see a warning that clearing cache will destroy data 
## brisk turkey number
- user must download zip archive of the dataset
