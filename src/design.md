# Design

GUI client for csvs, client for GEDCOM

competes: genea.app, GRAMPS, myheritage, familytree

interacts: with local storage, git hosting providers, social media API

constitutes: local and public a web application, mobile application for iOS and Android, desktop application for Linux, MacOS and Windows

includes: on the view side UI, UI state storage, overview components, import/export controllers; on the logic side a class that interacts with filesystem, csvs, git, git service providers

patterns: MVC 

resembles: matrix.ai, genea.app

stakeholders: fetsorn, 15 customers
 
target audiences: genealogists, family archivists, animal breeders
